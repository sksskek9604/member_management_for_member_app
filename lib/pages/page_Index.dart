import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_drawer/flutter_advanced_drawer.dart';
import 'package:member_management_for_member_app/components/component_company_news_item_form.dart';
import 'package:member_management_for_member_app/components/component_custom_drawer.dart';
import 'package:member_management_for_member_app/components/component_custom_loading.dart';
import 'package:member_management_for_member_app/components/component_no_contents.dart';
import 'package:member_management_for_member_app/components/component_notification.dart';
import 'package:member_management_for_member_app/model/company_news_item.dart';
import 'package:member_management_for_member_app/repository/repo_company_news.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({Key? key}) : super(key: key);

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  final _advancedDrawerController = AdvancedDrawerController();
  final _scrollController = ScrollController();

  List<CompanyNewsItem> _list = [];
  int _currentPage = 1;
  int _totalPage = 1;
  int _totalItemCount = 0;


  CompanyNewsItem? _companyNewsItem;

  @override
  void initState() {
    super.initState();

    _scrollController.addListener(() {
      if (_scrollController.offset ==
          _scrollController.position.maxScrollExtent) {
        _getCompanyNewsListAll();
      } // 바닥에 부딪치면 로드아이템 실행시켜라
    });
    _getCompanyNewsListAll();
  }

  Future<void> _getCompanyNewsListAll() async {
    BotToast.showCustomLoading(
        toastBuilder: (cancelFunc)
        => ComponentCustomLoading(cancelFunc: cancelFunc)
    );

    await RepoCompanyNews()
        .getCompanyNewsAll()
        .then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _list = res.list;
        _totalItemCount = res.totalItemCount;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      Navigator.pop(context);

      ComponentNotification(
        success: false,
        title: '불러오기 실패',
        subTitle: '데이터 불러오기에 실패했습니다.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return AdvancedDrawer(
        backdropColor: Colors.teal,
        controller: _advancedDrawerController,
        animationCurve: Curves.easeInOut,
        animationDuration: const Duration(milliseconds: 300),
        animateChildDecoration: true,
        rtlOpening: false,
        disabledGestures: false,
        childDecoration: const BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(16)),
        ),
        drawer: ComponentCustomDrawer(),
        child: Stack(
          children: [
            Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                        opacity: 1.0,
                        fit: BoxFit.fill,
                        image: AssetImage('assets/background.png'))),
                child:Scaffold(
                  backgroundColor: Colors.transparent,
                  appBar: AppBar(
                    title: const Text('오늘도 행복한 하루 보내세요!'),
                    leading: IconButton(
                      onPressed: _handleMenuButtonPressed,
                      icon: ValueListenableBuilder<AdvancedDrawerValue>(
                        valueListenable: _advancedDrawerController,
                        builder: (_, value, __) {
                          return AnimatedSwitcher(
                            duration: Duration(milliseconds: 250),
                            child: Icon(
                              value.visible ? Icons.arrow_back_ios : Icons.menu,
                              key: ValueKey<bool>(value.visible),
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                  // Todo appBar를 컴포넌트로 쓰면 넘어가질 않는다.. 1. 컴포넌트를 더 잘만들어본다. 2. 우선 컬러만 같게 지정해주는걸로 만족한다
                  body: ListView(
                    controller: _scrollController,
                    children: [
                      _buildBody(),
                    ],
                  ),
                )
            )
          ],
        )
    );
  }



  Widget _buildBody() {
    if (_totalItemCount > 0) {
      return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        ListView.builder(
          physics: const NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: _list.length,
          itemBuilder: (_, index) =>
              ComponentCompanyNewsItemForm(
                  item: _list[index],
                  callback: () {}),
        )
      ],
    );
    } else {
      return SizedBox(
        height: MediaQuery
            .of(context)
            .size
            .height -
            50 -
            20, // 기본 -50 ComponentsCountTitle 영역까지 빼기 -20
        child: const ComponentNoContents(
            icon: Icons.add_alert, msg: '뉴스 정보가 없습니다.'),
      );
    }
  }


  void _handleMenuButtonPressed() {
    _advancedDrawerController.showDrawer();
  }

  @override
  bool get wantKeppAlive => true;
}