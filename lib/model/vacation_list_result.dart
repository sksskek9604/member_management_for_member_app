import 'package:member_management_for_member_app/model/vacation_item.dart';

class VacationListResult {

  int totalItemCount;
  int totalPage;
  int currentPage;
  bool isSuccess;
  int code;
  String msg;
  List<VacationItem> list;

  VacationListResult(
      this.totalItemCount,
      this.totalPage,
      this.currentPage,
      this.isSuccess,
      this.code,
      this.msg,
      this.list
      );

  factory VacationListResult.fromJson(Map<String, dynamic> json) {
    return VacationListResult(
      json['totalItemCount'] as int,
      json['totalPage'] as int ,
      json['currentPage']as int ,
      json['isSuccess'] as bool ,
      json['code'] as int,
      json['msg'],
      json['list'] == null? [] : (json['list'] as List).map((e) => VacationItem.fromJson(e)).toList(),

    );
  }
}