class LoginUserResponse {
  int memberId;

  LoginUserResponse(this.memberId);

  factory LoginUserResponse.fromJson(Map<String, dynamic> json) {
    return LoginUserResponse( // 이미 id로 깔끔하게 숫자를 주기 때문에 케스팅하지 않아도 됨
      json['memberId']
    );
  }
}