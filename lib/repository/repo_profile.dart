import 'dart:io';


import 'package:dio/dio.dart';
import 'package:member_management_for_member_app/config/config_api.dart';
import 'package:member_management_for_member_app/functions/token_lib.dart';
import 'package:member_management_for_member_app/model/common_result.dart';
import 'package:member_management_for_member_app/model/member/login_user_response.dart';
import 'package:member_management_for_member_app/model/profile/profile_image_result.dart';

class RepoProfile {
  Future<ProfileImageResult> getImage() async {
    String baseUrl = '$apiUri/profile/image/member-id/{memberId}';

    LoginUserResponse? token = (await TokenLib.getToken()) as LoginUserResponse?;

    Dio dio = Dio();

    try {
      final response = await dio.get(
        baseUrl.replaceAll('{memberId}', token!.memberId.toString()));
      return ProfileImageResult.fromJson(response.data);
    } on DioError catch (e) {
      return ProfileImageResult.fromJson(e.response!.data);
    }
  }

  Future<CommonResult> doUpload(File file) async {
    String baseUrl = '$apiUri/profile/image/member-id/{memberId}';

    LoginUserResponse? token = (await TokenLib.getToken()) as LoginUserResponse?;

    Dio dio = Dio();

    try {
      FormData formData = FormData.fromMap({
        'file': MultipartFile.fromFile(file.path)
      });

      final response = await dio.post(
          baseUrl.replaceAll('{memberId}', token!.memberId.toString()),
          data: formData,

      );

      return CommonResult.fromJson(response.data);
    } on DioError catch (e) {
      return CommonResult.fromJson(e.response!.data);
    }
  } //Todo 이후 CDN을 통한 연동 예정
}
