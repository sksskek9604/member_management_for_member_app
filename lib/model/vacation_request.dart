class VacationRequest{
  String vacationType;
  String vacationStart;
  String vacationEnd;
  String vacationReason;
  // bool isMinus;
  String increaseOrDecreaseValue;

  VacationRequest(
      this.vacationType, this.vacationStart, this.vacationEnd, this.vacationReason, this.increaseOrDecreaseValue
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    data['vacationType'] = this.vacationType;
    data['vacationStart'] = this.vacationStart;
    data['vacationEnd'] = this.vacationEnd;
    data['vacationReason'] = this.vacationReason;
    // data['isMinus'] = this.isMinus;
    data['increaseOrDecreaseValue'] = this.increaseOrDecreaseValue;

    return data;

  }


}