import 'package:blurrycontainer/blurrycontainer.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_drawer/flutter_advanced_drawer.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:intl/intl.dart';
import 'package:member_management_for_member_app/components/component_custom_drawer.dart';
import 'package:member_management_for_member_app/components/component_custom_loading.dart';
import 'package:member_management_for_member_app/components/component_notification.dart';
import 'package:member_management_for_member_app/config/config_dropdown.dart';
import 'package:member_management_for_member_app/config/config_form_validator.dart';
import 'package:member_management_for_member_app/model/vacation_request.dart';
import 'package:member_management_for_member_app/repository/repo_vacation.dart';
class PageVacationRequest extends StatefulWidget {
  const PageVacationRequest({Key? key}) : super(key: key);

  @override
  State<PageVacationRequest> createState() => _PageVacationRequestState();
}

class _PageVacationRequestState extends State<PageVacationRequest> {

  final _advancedDrawerController = AdvancedDrawerController();
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _setVacation(VacationRequest request) async {
    // 여기서부터
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });
    // 여기까지 커스텀로딩 띄우는 소스

    await RepoVacation().setData(request).then((res) {
      BotToast.closeAllLoading(); // 커스텀로딩 닫고

      ComponentNotification(
        success: true,
        title: '연차 신청 성공',
        subTitle: res.msg, // 백엔드에서 반환받은 메세지 넣기
      ).call();

      // 창 닫으면서 다 처리하고 정상적으로 닫혔다!! 라고 알려주기
      Navigator.pop(
          context,
          [true] // <- 이부분.. 다 처리하고 정상적으로 닫혔다!!
      );
    }).catchError((err) {
      BotToast.closeAllLoading(); // 커스텀로딩 닫고

      ComponentNotification(
        success: false,
        title: '연차 신청 실패',
        subTitle: '연차 신청에 실패하였습니다.',
      ).call();
    });
  }


  @override
  Widget build(BuildContext context) {
    return AdvancedDrawer(
        backdropColor: Colors.teal,
        controller: _advancedDrawerController,
        animationCurve: Curves.easeInOut,
        animationDuration: const Duration(milliseconds: 300),
        animateChildDecoration: true,
        rtlOpening: false,
        disabledGestures: false,
        childDecoration: const BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(16)),
        ),
        drawer: ComponentCustomDrawer(),
        child: Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                  image: DecorationImage(
                      opacity: 1.0,
                      fit: BoxFit.fill,
                      image: AssetImage('assets/background.png'))),
              child: Scaffold(
                backgroundColor: Colors.transparent,
                appBar: AppBar(
                  title: const Text('오늘도 행복한 하루 보내세요!'),
                  leading: IconButton(
                    onPressed: _handleMenuButtonPressed,
                    icon: ValueListenableBuilder<AdvancedDrawerValue>(
                      valueListenable: _advancedDrawerController,
                      builder: (_, value, __) {
                        return AnimatedSwitcher(
                          duration: Duration(milliseconds: 250),
                          child: Icon(
                            value.visible ? Icons.arrow_back_ios : Icons.menu,
                            key: ValueKey<bool>(value.visible),
                          ),
                        );
                      },
                    ),
                  ),
                ),
                // Todo appBar를 컴포넌트로 쓰면 넘어가질 않는다.. 1. 컴포넌트를 더 잘만들어본다. 2. 우선 컬러만 같게 지정해주는걸로 만족한다
                body: _buildBody(context),
              ),
            )
          ],
        )
    );
  }

  Widget _buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.all(30),
        child: FormBuilder(
          key: _formKey,
          autovalidateMode: AutovalidateMode.disabled,
          child: Column(
            children: [
              FormBuilderDropdown<String>(
                name: 'vacationType',
                decoration: const InputDecoration(
                  labelText: '휴가 신청 종류',
                ),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired)
                ]),
                items: dropdownVacationType,
              ),// 휴가 종류
              FormBuilderTextField(
                name: 'increaseOrDecreaseValue',
                decoration: const InputDecoration(
                  labelText: '증감값',
                ),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                ]),
                keyboardType: TextInputType.number,
              ), // 증감값
              FormBuilderTextField(
                name: 'vacationReason',
                decoration: const InputDecoration(
                  labelText: '신청 사유',
                ),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                  FormBuilderValidators.minLength(2, errorText: formErrorMinLength(2)),
                  FormBuilderValidators.maxLength(15, errorText: formErrorMaxLength(15)),// 이 값은 필수입니다.
                ]),
                keyboardType: TextInputType.text,
              ), //휴가 사유
              FormBuilderDateTimePicker(
                name: 'vacationStart',
                format: DateFormat('yyyy-MM-dd'),
                inputType: InputType.date,
                decoration: InputDecoration(
                  labelText: '휴가 시작일',
                  suffixIcon: IconButton(
                    icon: const Icon(Icons.calendar_month),
                    onPressed: () {
                      _formKey.currentState!.fields['vacationStart']
                          ?.didChange(null);
                    },
                  ),
                ),
                locale: const Locale.fromSubtags(languageCode: 'ko'),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired)
                ]),
              ), // 휴가 시작일
              FormBuilderDateTimePicker(
                name: 'vacationEnd',
                format: DateFormat('yyyy-MM-dd'),
                inputType: InputType.date,
                decoration: InputDecoration(
                  labelText: '휴가 마지막일',
                  suffixIcon: IconButton(
                    icon: const Icon(Icons.calendar_month),
                    onPressed: () {
                      _formKey.currentState!.fields['vacationEnd']
                          ?.didChange(null);
                    },
                  ),
                ),
                locale: const Locale.fromSubtags(languageCode: 'ko'),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired)
                ]),
              ), // 휴가 마지막일
              SizedBox(height: 30,),
              OutlinedButton(
                onPressed: () {
                  if (_formKey.currentState?.saveAndValidate() ?? false) {
                    VacationRequest request = VacationRequest(
                      _formKey.currentState!.fields['vacationType']!.value,
                      DateFormat('yyyy-MM-dd').format(_formKey.currentState!.fields['vacationStart']!.value),
                      DateFormat('yyyy-MM-dd').format(_formKey.currentState!.fields['vacationEnd']!.value),
                      _formKey.currentState!.fields['vacationReason']!.value,
                      _formKey.currentState!.fields['increaseOrDecreaseValue']!.value,

                    );
                    _setVacation(request);
                  }
                },
                child: Text('등록'),
              ),// 휴가 마지막일
            ],
          ),
        ),
      )
    );
  }
  void _handleMenuButtonPressed() {
    _advancedDrawerController.showDrawer();
  }
}
