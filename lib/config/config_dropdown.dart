import 'package:flutter/material.dart';

const List<DropdownMenuItem<String>> dropdownVacationType = [
  DropdownMenuItem(value: 'ANNUAL_LEAVE', child: Text('연차')),
  DropdownMenuItem(value: 'MONTHLY_LEAVE', child: Text('월차')),
  DropdownMenuItem(value: 'HALF_DAY_LEAVE', child: Text('반차')),
  DropdownMenuItem(value: 'SICK_LEAVE', child: Text('병가')),
  DropdownMenuItem(value: 'ANNUAL_LEAVE_PLUS', child: Text('연차 추가')),


];