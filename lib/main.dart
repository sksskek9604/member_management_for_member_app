import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:member_management_for_member_app/login_check.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:member_management_for_member_app/pages/page_Index.dart';
import 'package:member_management_for_member_app/pages/page_dailycheck.dart';
import 'package:member_management_for_member_app/pages/page_login.dart';
import 'package:member_management_for_member_app/pages/page_profile_form.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});


  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp
    ]);
    return MaterialApp(
      title: '회사 app',
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      builder: BotToastInit(),
      navigatorObservers: [
        BotToastNavigatorObserver(),
      ],
      theme: ThemeData(
        fontFamily: 'KR',
        primarySwatch: Colors.teal,
        brightness: Brightness.light,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        primaryColor: Colors.blueGrey,
      ),

      // 여기에 page_index 가 들어가버리면 무조건 비회원용 메인으로 가기 때문에
      // token 값을 검사하여 token이 있으면 회원용 메인, 없으면 비회원용 메인으로 가게끔 해야 함.
      home: const LoginCheck(),
    );
  }
}



