import 'package:member_management_for_member_app/model/member/login_user_response.dart';

class LoginResult {
  LoginUserResponse data;
  bool isSuccess;
  int code;
  String msg;

  LoginResult(this.data, this.isSuccess, this.code, this.msg);

  factory LoginResult.fromJson(Map<String, dynamic> json) {
    return LoginResult(
        LoginUserResponse.fromJson(json['data']),
        json['isSuccess'] as bool,
        json['code'],
        json['msg']);
  }
}