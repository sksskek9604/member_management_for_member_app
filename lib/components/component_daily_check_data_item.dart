import 'package:flutter/material.dart';
import 'package:member_management_for_member_app/config/config_size.dart';
import 'package:member_management_for_member_app/model/daily_check/daily_check_list_item.dart';

class ComponentDailyCheckDataItem extends StatelessWidget {
  const ComponentDailyCheckDataItem({super.key, required this.dailyCheckListItem});

  final DailyCheckListItem dailyCheckListItem;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              border: Border.all(width: 2.0, color: Colors.black45),
              borderRadius: BorderRadius.circular(20.0),
            ),
            padding: EdgeInsets.all(30),
            margin: EdgeInsets.all(30),
            child: Column(
              children: [
                Text('날짜 : ${dailyCheckListItem.dateBase} ',style: TextStyle(fontSize: 25, color: Colors.teal.shade700),),
                Divider(),
                Text('출근 시간 : ${dailyCheckListItem.dateWorkStart} ', style: TextStyle(fontSize: fontSizeBig),),
                Text('퇴근 시간 : ${dailyCheckListItem.dateWorkEnd} ', style: TextStyle(fontSize: fontSizeBig),),
                Text('조퇴 시간 : ${dailyCheckListItem.dateEarlyLeave} ', style: TextStyle(fontSize: fontSizeBig),),
                Text('외출시간 : ${dailyCheckListItem.dateShortOuting} ', style: TextStyle(fontSize: fontSizeBig),),
                Text('외출 복귀 시간 : ${dailyCheckListItem.dateWorkComeBack} ', style: TextStyle(fontSize: fontSizeBig),),
                Text('상태 : ${dailyCheckListItem.dailyCheckState} ', style: TextStyle(fontSize: fontSizeBig),),
              ],
            ),
          )
        ],
      ),
    );
  }
}
