import 'package:animated_button/animated_button.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:member_management_for_member_app/components/appbar/component_appbar_normal.dart';
import 'package:member_management_for_member_app/components/component_custom_loading.dart';
import 'package:member_management_for_member_app/components/component_notification.dart';
import 'package:member_management_for_member_app/config/config_form_validator.dart';
import 'package:member_management_for_member_app/functions/token_lib.dart';
import 'package:member_management_for_member_app/middleware/middleware_login_check.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:member_management_for_member_app/model/member/login_user_request.dart';
import 'package:member_management_for_member_app/repository/repo_member.dart';

class PageLogin extends StatefulWidget {
  const PageLogin({Key? key}) : super(key: key);

  @override
  State<PageLogin> createState() => _PageLoginState();
}

class _PageLoginState extends State<PageLogin> {

  bool autoValidate = true;
  bool readOnly = false;
  bool showSegmentedControl = true;
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _doLogin(LoginUserRequest request) async{
    //api콜하기 전에 잠깐만 기다려!라는거 넣어줘야죠?
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMember().doLogin(request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '로그인 성공',
        subTitle: '로그인에 성공하였습니다',
      ).call();

      // api에서 받아온 결과값을 token에 넣는다.
      TokenLib.setToken(res.data);

      // 미들웨어에게 부탁해서 토큰값 여부 검사 후 페이지 이동을 부탁한다.
      MiddleWareLoginCheck().check(context);

    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '로그인 실패',
        subTitle: '아이디 혹은 비밀번호를 확인해주세요.', // 해킹때문에 요즘 추세는 뭉뜽그림
      ).call();
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: ComponentAppbarNormal(title: '로그인',),
        body: Container(
          padding: EdgeInsets.all(40.0),
          child: Center(
            child: Column(
              children: [
                _buildBody(),
              ],
            ),
          ),
        ));
  }
  Widget _buildBody() {
    return SingleChildScrollView(
      child: FormBuilder(
        key: _formKey,
        autovalidateMode: AutovalidateMode.disabled,
        child: Column(
          children: [
            Padding(padding: EdgeInsets.all(10),
            child:FormBuilderTextField(
              name: 'username',
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: '아이디',
              ),
              validator: FormBuilderValidators.compose([
                FormBuilderValidators.required(errorText: formErrorRequired),
                FormBuilderValidators.minLength(8, errorText: formErrorMinLength(8)),
                FormBuilderValidators.maxLength(15, errorText: formErrorMaxLength(15)),
              ]),
              keyboardType: TextInputType.text,
              style: TextStyle(color: Colors.teal),
            ),),
            Padding(padding: EdgeInsets.all(10),
            child: FormBuilderTextField(
              name: 'password',
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: '비밀번호',
              ),
              obscureText: true, // 패스워드 모드 어떻게 넣는지 검색
                obscuringCharacter: '*', // 폰트 바꿨더니 안보이는 현상이 생겨서 추가함.
                style: TextStyle(color: Colors.teal),
              validator: FormBuilderValidators.compose([
                FormBuilderValidators.required(errorText: formErrorRequired),
                FormBuilderValidators.minLength(8, errorText: formErrorMinLength(8)),
                FormBuilderValidators.maxLength(15, errorText: formErrorMaxLength(15)),
              ]),
              keyboardType: TextInputType.text,
            ),),
            SizedBox(height: 40.0,),
            AnimatedButton(
              duration: 50,
                width: 300,
                height: 50,
                color: Colors.teal,
                shadowDegree: ShadowDegree.dark,
                onPressed: () {
                  if (_formKey.currentState?.saveAndValidate() ?? false) {
                    LoginUserRequest loginRequest = LoginUserRequest(
                      _formKey.currentState!.fields['username']!.value,
                      _formKey.currentState!.fields['password']!.value,
                    );
                    _doLogin(loginRequest);
                  }
                },
                child: Text('Login',style: TextStyle(
                  fontSize: 30,
                  color: Colors.white,
                  fontWeight: FontWeight.w500,
                ),
                ),
            ),
            Container(
              padding: EdgeInsets.all(30),
              child: Column(
                children: [
                  Text('J&S company app'
                  ,style: TextStyle(color: Colors.blueGrey),)
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
