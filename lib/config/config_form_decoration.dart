import 'package:flutter/material.dart';

class ConfigFormDecoration {
  InputDecoration getSwitchDecoration() {
    return InputDecoration(
      isCollapsed: true,
      isDense: true,
      errorBorder: InputBorder.none,
      focusedBorder: InputBorder.none,
      focusedErrorBorder: InputBorder.none,
      disabledBorder: InputBorder.none,
      enabledBorder: InputBorder.none,
      border: InputBorder.none,
    );
  }

  InputDecoration getCheckBoxDecoration() {
    return InputDecoration(
      isCollapsed: true,
      isDense: true,
      errorBorder: InputBorder.none,
      focusedBorder: InputBorder.none,
      focusedErrorBorder: InputBorder.none,
      disabledBorder: InputBorder.none,
      enabledBorder: InputBorder.none,
      border: InputBorder.none,
    );
  }

  InputDecoration getDateBoxDecoration(String inputName) {
    return InputDecoration(
      labelText: '$inputName',
      hintText: '날짜선택',
      contentPadding: const EdgeInsets.all(0),
      floatingLabelBehavior: FloatingLabelBehavior.always,
      errorBorder: InputBorder.none,
      focusedBorder: InputBorder.none,
      focusedErrorBorder: InputBorder.none,
      disabledBorder: InputBorder.none,
      enabledBorder: InputBorder.none,
      border: InputBorder.none,
      errorStyle: TextStyle(
        height: 0.5,
      ),
      isDense: false,
    );
  }

  InputDecoration getInputDecoration(String inputName, {bool useSuffixText = false, String suffixText = ''}) {
    return InputDecoration(
      suffixText: useSuffixText ? suffixText : null,
      labelText: '$inputName',
      contentPadding: const EdgeInsets.all(0),
      floatingLabelBehavior: FloatingLabelBehavior.always,
      errorBorder: InputBorder.none,
      focusedBorder: InputBorder.none,
      focusedErrorBorder: InputBorder.none,
      disabledBorder: InputBorder.none,
      enabledBorder: InputBorder.none,
      border: InputBorder.none,
      errorStyle: TextStyle(
        height: 0.5,
      ),
      isDense: false,
      counter: SizedBox(
        height: 0.0,
      ),
    );
  }

  InputDecoration getInputSearchDecoration(String inputName) {
    return InputDecoration(
      labelText: '$inputName',
      contentPadding: const EdgeInsets.all(0),
      floatingLabelBehavior: FloatingLabelBehavior.always,
      errorBorder: InputBorder.none,
      focusedBorder: InputBorder.none,
      focusedErrorBorder: InputBorder.none,
      disabledBorder: InputBorder.none,
      enabledBorder: InputBorder.none,
      border: InputBorder.none,
      errorStyle: TextStyle(
        height: 0.5,
      ),
      isDense: false,
      counter: SizedBox(
        height: 0.0,
      ),
    );
  }

}