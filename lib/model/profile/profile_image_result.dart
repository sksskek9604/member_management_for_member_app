import 'package:member_management_for_member_app/model/profile/profile_image_response.dart';

class ProfileImageResult {
  bool isSuccess;
  int code;
  String msg;
  ProfileImageResponse? data;

  ProfileImageResult(this.isSuccess, this.code, this.msg, {this.data});

  factory ProfileImageResult.fromJson(Map<String, dynamic> json) {
    return ProfileImageResult(
      json['isSuccess'] as bool,
      json['code'],
      json['msg'],
      data: json['data'] == null
          ? null
          : ProfileImageResponse.fromJson(json['data']),
    );
  }
}
