import 'package:blurrycontainer/blurrycontainer.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_drawer/flutter_advanced_drawer.dart';
import 'package:member_management_for_member_app/components/component_custom_drawer.dart';
import 'package:member_management_for_member_app/components/component_custom_loading.dart';
import 'package:member_management_for_member_app/repository/repo_daily_check.dart';

class PageDailyCheck extends StatefulWidget {
  const PageDailyCheck({Key? key}) : super(key: key);

  @override
  State<PageDailyCheck> createState() => _PageDailyCheckState();
}

class _PageDailyCheckState extends State<PageDailyCheck> {
  String _dailyCheckStateName = '';
  String _dailyCheckState = '';
  final _advancedDrawerController = AdvancedDrawerController();

  @override
  void initState() {
    super.initState();
    _getData();
  }

  Future<void> _getData() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoDailyCheck().getData().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _dailyCheckStateName = res.data!.dailyCheckStateName;
        _dailyCheckState = res.data!.dailyCheckState;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();
    });
  }

  Future<void> _putData(String dailyCheckState) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoDailyCheck().putState(dailyCheckState).then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _dailyCheckStateName = res.data!.dailyCheckStateName;
        _dailyCheckState = res.data!.dailyCheckState;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      print(err);
    });
  }


  @override
  Widget build(BuildContext context) {
    return AdvancedDrawer(
        backdropColor: Colors.teal,
        controller: _advancedDrawerController,
        animationCurve: Curves.easeInOut,
        animationDuration: const Duration(milliseconds: 300),
        animateChildDecoration: true,
        rtlOpening: false,
        disabledGestures: false,
        childDecoration: const BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(16)),
        ),
        drawer: ComponentCustomDrawer(),
        child: Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                  image: DecorationImage(
                      opacity: 1.0,
                      fit: BoxFit.fill,
                      image: AssetImage('assets/background.png'))),
              child: Scaffold(
                backgroundColor: Colors.transparent,
                appBar: AppBar(
                  title: const Text('오늘도 좋은 하루 되세요!'),
                  leading: IconButton(
                    onPressed: _handleMenuButtonPressed,
                    icon: ValueListenableBuilder<AdvancedDrawerValue>(
                      valueListenable: _advancedDrawerController,
                      builder: (_, value, __) {
                        return AnimatedSwitcher(
                          duration: Duration(milliseconds: 250),
                          child: Icon(
                            value.visible ? Icons.arrow_back_ios : Icons.menu,
                            key: ValueKey<bool>(value.visible),
                          ),
                        );
                      },
                    ),
                  ),
                ),
                body: _buildBody(context),
              ),
            )
          ],
        )
    );
  }

  Widget _buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Center(
        child: Container(
          padding: EdgeInsets.all(5),
          child: Column(
            children: [
              BlurryContainer(
                blur: 10,
                width: 370,
                height: 190,
                elevation: 9,
                color: Colors.tealAccent.shade100.withOpacity(0.6),
                padding: EdgeInsets.only(left: 20,right: 20, top: 10, bottom: 20),
                child: Column(
                  children: [
                    Center(
                      child:Row(
                        children: [
                          Icon(Icons.directions_walk, size: 50, color: Colors.black12,),
                          Text('현재 상태 ' , style: TextStyle(fontSize: 50, color: Colors.teal.shade800),),
                        ],
                      ),
                    ),
                    Divider(),
                    Center(
                      child: Text('$_dailyCheckStateName', style: TextStyle(fontSize: 70, color: Colors.tealAccent.shade700),),
                    ),
                  ],
                ),
              ),
              Divider(),
              SizedBox(height: 30,),
              if (_dailyCheckState == 'UNKNOWN')
                OutlinedButton(
                    style: OutlinedButton.styleFrom(
                      padding: EdgeInsets.only(top: 30, bottom: 30, left: 40, right: 40),
                      shape: BeveledRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(25))),
                      side: BorderSide(width: 5.0, color: Colors.teal),
                    ),
                    onPressed: () {
                      _putData('WORK_START');
                    },
                    child: Text('출근', style: TextStyle(fontSize: 30, color: Colors.teal),)
                ),
              SizedBox(height: 30,),
              if (_dailyCheckState == 'WORK_START')
                OutlinedButton(
                  style: OutlinedButton.styleFrom(
                    padding: EdgeInsets.only(top: 30, bottom: 30, left: 40, right: 40),
                    shape: BeveledRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(25))),
                    side: BorderSide(width: 5.0, color: Colors.teal),
                  ),
                    onPressed: () {
                      _putData('SHORT_OUTING');
                    },
                    child: Text('외출', style: TextStyle(fontSize: 30, color: Colors.teal),)
                ),
              SizedBox(height: 30,),
              if (_dailyCheckState == 'WORK_START')
                OutlinedButton(
                    style: OutlinedButton.styleFrom(
                      padding: EdgeInsets.only(top: 30, bottom: 30, left: 40, right: 40),
                      shape: BeveledRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(25))),
                      side: BorderSide(width: 5.0, color: Colors.teal),
                    ),
                    onPressed: () {
                      _putData('TIME_EARLY_LEAVE');
                    },
                    child: Text('조퇴', style: TextStyle(fontSize: 30, color: Colors.teal),)
                ),
              SizedBox(height: 30,),
              if (_dailyCheckState == 'WORK_START')
                OutlinedButton(
                    style: OutlinedButton.styleFrom(
                      padding: EdgeInsets.only(top: 30, bottom: 30, left: 40, right: 40),
                      shape: BeveledRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(25))),
                      side: BorderSide(width: 5.0, color: Colors.teal),
                    ),
                    onPressed: () {
                      _putData('WORK_END');
                    },
                    child: Text('퇴근', style: TextStyle(fontSize: 30, color: Colors.teal),)
                ),
              SizedBox(height: 30,),
              if (_dailyCheckState == 'SHORT_OUTING')
                OutlinedButton(
                    style: OutlinedButton.styleFrom(
                      padding: EdgeInsets.only(top: 30, bottom: 30, left: 40, right: 40),
                      shape: BeveledRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(25))),
                      side: BorderSide(width: 5.0, color: Colors.teal),
                    ),
                    onPressed: () {
                      _putData('COME_BACK');
                    },
                    child: Text('외출 복귀', style: TextStyle(fontSize: 30, color: Colors.teal),)
                ),
              SizedBox(height: 30,),
              if (_dailyCheckState == 'COME_BACK')
                OutlinedButton(
                    style: OutlinedButton.styleFrom(
                      padding: EdgeInsets.only(top: 30, bottom: 30, left: 40, right: 40),
                      shape: BeveledRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(25))),
                      side: BorderSide(width: 5.0, color: Colors.teal),
                    ),
                    onPressed: () {
                      _putData('WORK_END');
                    },
                    child: Text('퇴근', style: TextStyle(fontSize: 30, color: Colors.teal),)
                ),
              SizedBox(height: 30,),
              if (_dailyCheckState == 'COME_BACK')
                OutlinedButton(
                    style: OutlinedButton.styleFrom(
                      padding: EdgeInsets.only(top: 30, bottom: 30, left: 40, right: 40),
                      shape: BeveledRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(25))),
                      side: BorderSide(width: 5.0, color: Colors.teal),
                    ),
                    onPressed: () {
                      _putData('TIME_EARLY_LEAVE');
                    },
                    child: Text('조퇴', style: TextStyle(fontSize: 30, color: Colors.teal),)
                ),

              if (_dailyCheckState == 'WORK_END' || _dailyCheckState == 'WORK_END'|| _dailyCheckState == 'TIME_EARLY_LEAVE')
                Text('퇴근 후에는\n상태를 변경 \n할 수없습니다.', style: TextStyle(fontSize: 40),),
            ],
          ),
        )
      )
    );
  }
  void _handleMenuButtonPressed() {
    _advancedDrawerController.showDrawer();
  }
}
