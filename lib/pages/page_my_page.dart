import 'dart:io';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_drawer/flutter_advanced_drawer.dart';
import 'package:image_picker/image_picker.dart';
import 'package:member_management_for_member_app/components/component_custom_drawer.dart';
import 'package:member_management_for_member_app/components/component_custom_loading.dart';
import 'package:member_management_for_member_app/components/component_my_page_item_property.dart';
import 'package:member_management_for_member_app/components/component_notification.dart';
import 'package:member_management_for_member_app/model/member/my_page_item.dart';
import 'package:member_management_for_member_app/pages/page_profile_form.dart';
import 'package:member_management_for_member_app/repository/repo_member.dart';
import 'package:member_management_for_member_app/repository/repo_profile.dart';
import 'package:widget_circular_animator/widget_circular_animator.dart';

class PageMyPage extends StatefulWidget {
  const PageMyPage({Key? key}) : super(key: key);

  @override
  State<PageMyPage> createState() => _PageMyPageState();
}

class _PageMyPageState extends State<PageMyPage> {
  final _advancedDrawerController = AdvancedDrawerController();

  final ImagePicker _picker = ImagePicker();
  final TextEditingController maxWidthController = TextEditingController();
  final TextEditingController maxHeightController = TextEditingController();
  final TextEditingController qualityController = TextEditingController();

  MyPageItem? _myPageItem;

  XFile? _imageFile; // 갤러리에서 사진 선택한것 소스코드.. 일단 null로 있다가 선택된걸 집어넣는 변수.

  String _currentImageUrl = '';
  String _imageUploadTime = '';


  @override
  void initState() {
    super.initState();
    _getCurrentImage();
    _getMyPageResponse();
  }

  Future<void> _getMyPageResponse() async {
    BotToast.showCustomLoading(
        toastBuilder: (cancelFunc)
        => ComponentCustomLoading(cancelFunc: cancelFunc)
    );

    await RepoMember().getMemberItem().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _myPageItem = res.data;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      Navigator.pop(context);

      ComponentNotification(
        success: false,
        title: '불러오기 실패',
        subTitle: '데이터 불러오기에 실패했습니다.',
      ).call();
    });
  }

  // Todo 업로드 기능 수정 필요
  // 현재 프로필 이미지 정보 가져오는 메서드
  Future<void> _getCurrentImage() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoProfile().getImage().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _currentImageUrl = res.data!.imageName;
        _imageUploadTime = res.data!.dateUpload;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();
    });
  }

  Future<void> _callGallery({BuildContext? context}) async {
    try {
      final XFile? pickedFile = await _picker.pickImage(
        source: ImageSource.gallery,
        maxWidth: 2000, // 갤러리에서 선택할 사진의 최대 폭 (폭이나 넓이가 크다 = 사진용량이 크다 = 서버터진다)
        maxHeight: 2000, // 갤러리에서 선택할 사진의 최대 높이
        imageQuality: 100, // 사진퀄리티 (0~100, 높을수록 사진 용량 늘어남)
      );
      setState(() {
        // 사진선택이 완료되면 선택된 사진을 사진리스트 변수에 넣어둠 : 타입 XFile
        _imageFile = pickedFile;
      });

      File file = File(_imageFile!.path); // XFile(패키지에서 쓰는 자체 타입) 을 File로 변환함.
      _doUploadImage(file); // 사진 파일을 업로드 시킴
    } catch (e) {
      ComponentNotification(
        success: false,
        title: '갤러리 호출 취소',
        subTitle: '갤러리 호출을 취소하였습니다.',
      ).call();
    }
  }

  Future<void> _doUploadImage(File file) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoProfile().doUpload(file).then((res) {
      BotToast.closeAllLoading();

      _getCurrentImage(); // 사진 업로드에 성공하면 다시 한번 api로 현재 프로필 이미지 정보를 달라고 요청함.
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '이미지 업로드 실패',
        subTitle: '이미지 업로드에 실패하였습니다.',
      ).call();
    });
  }


  @override
  Widget build(BuildContext context) {
    return AdvancedDrawer(
        backdropColor: Colors.teal,
        controller: _advancedDrawerController,
        animationCurve: Curves.easeInOut,
        animationDuration: const Duration(milliseconds: 300),
        animateChildDecoration: true,
        rtlOpening: false,
        disabledGestures: false,
        childDecoration: const BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(16)),
        ),
        drawer: ComponentCustomDrawer(),
        child: Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                  image: DecorationImage(
                      opacity: 1.0,
                      fit: BoxFit.fill,
                      image: AssetImage('assets/background.png'))),
              child: Scaffold(
                backgroundColor: Colors.transparent,
                appBar: AppBar(
                  title: const Text('오늘도 좋은 하루 되세요!'),
                  leading: IconButton(
                    onPressed: _handleMenuButtonPressed,
                    icon: ValueListenableBuilder<AdvancedDrawerValue>(
                      valueListenable: _advancedDrawerController,
                      builder: (_, value, __) {
                        return AnimatedSwitcher(
                          duration: Duration(milliseconds: 250),
                          child: Icon(
                            value.visible ? Icons.arrow_back_ios : Icons.menu,
                            key: ValueKey<bool>(value.visible),
                          ),
                        );
                      },
                    ),
                  ),
                ),
                body: _buildBody(context),
              ),
            )
          ],
        )
    );
  }
  Widget _buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.all(30.0),
        child: Column(
          children: [
            Center(
              child: WidgetCircularAnimator(
                size: 300,
                innerIconsSize: 3,
                outerIconsSize: 3,
                innerAnimation: Curves.easeInOutBack,
                outerAnimation: Curves.easeInOutBack,
                innerColor: Colors.teal,
                outerColor: Colors.teal,
                innerAnimationSeconds: 10,
                outerAnimationSeconds: 10,
                child: Container(
                  child: Center(
                    child: Column(
                      children: [
                        if (_currentImageUrl == '')
                          CircleAvatar(
                            radius: 100,
                            child: Icon(Icons.account_circle, size: 200,),
                          ),
                        if (_currentImageUrl != '')
                          CircleAvatar(
                            radius: 100,
                            backgroundImage: NetworkImage(_currentImageUrl),
                          ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Center(
              child: SizedBox(
                width: 40,
                height: 40,
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(50),
                    child: GestureDetector(
                      child:Positioned(
                        bottom: 0,
                        right: 0,
                        child: Container(
                          width: 20,
                          height: 20,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            color: Colors.grey[100],
                          ),
                          child: Icon(
                            Icons.camera_alt_outlined,
                            size: 15,
                          ),
                        ),
                      ),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => PageProfileForm())
                        );
                      },
                    )
                ),
              ),
            ),
            ComponentMyPageItemProperty(title: '아이디', contents: _myPageItem?.username ?? ''),
            ComponentMyPageItemProperty(title: '정보', contents: _myPageItem?.fullName ?? ''),
            ComponentMyPageItemProperty(title: '주소', contents: _myPageItem?.memberAddress ?? ''),
            ComponentMyPageItemProperty(title: '연락처', contents: _myPageItem?.memberPhone ?? ''),
            ComponentMyPageItemProperty(title: '입사일', contents: _myPageItem?.dateJoin ?? ''),
          ],
        ),
      )
    );
  }
  void _handleMenuButtonPressed() {
    _advancedDrawerController.showDrawer();
  }

}
