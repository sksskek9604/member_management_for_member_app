import 'package:dio/dio.dart';
import 'package:member_management_for_member_app/config/config_api.dart';
import 'package:member_management_for_member_app/model/company_news_list_result.dart';

class RepoCompanyNews {

  Future<CompanyNewsListResult> getCompanyNewsAll() async {
    const String baseUrl = '$apiUri/company-news/all';

    Dio dio = Dio();

    final response = await dio.get(
      baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) => (status == 200)
        )
    );
    return CompanyNewsListResult.fromJson(response.data);
  }


}