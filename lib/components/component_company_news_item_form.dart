import 'package:flutter/material.dart';
import 'package:member_management_for_member_app/model/company_news_item.dart';

class ComponentCompanyNewsItemForm extends StatelessWidget {
  const ComponentCompanyNewsItemForm({super.key, required this.item, required this.callback});

  final CompanyNewsItem item;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        padding: const EdgeInsets.all(25),
        margin: const EdgeInsets.all(30),
        decoration: BoxDecoration(
          border: Border.all(color: const Color.fromRGBO(300, 300, 300, 100)),
          borderRadius: BorderRadius.all(Radius.circular(20))
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Icon(Icons.lightbulb_circle_sharp),
                Text('News : ' + '${item.id}', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),),
              ],),
            Divider(),
            Text('${item.headline}', style: TextStyle(fontSize: 25, fontWeight: FontWeight.w500, color: Colors.teal.shade700)),
            SizedBox(height: 20,),
            Text('${item.contents}', style: TextStyle(fontWeight: FontWeight.w500)),
        ]
        ),
      ),
    );
  }
}
