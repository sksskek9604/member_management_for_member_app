import 'package:flutter/material.dart';
import 'package:member_management_for_member_app/middleware/middleware_login_check.dart';


class LoginCheck extends StatefulWidget {
  const LoginCheck({Key? key}) : super(key: key);

  @override
  State<LoginCheck> createState() => _LoginCheckState();
}

class _LoginCheckState extends State<LoginCheck> {
  @override
  void initState() {
    super.initState();
    MiddleWareLoginCheck().check(context);
    // 토큰 유무를 검사하여 페이지 이동을 강제로 시켜버리는 미들웨어

  }
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
