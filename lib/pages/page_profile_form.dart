import 'dart:io';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:member_management_for_member_app/components/appbar/component_appbar_actions.dart';
import 'package:member_management_for_member_app/components/component_custom_loading.dart';
import 'package:member_management_for_member_app/components/component_notification.dart';
import 'package:member_management_for_member_app/repository/repo_profile.dart';

class PageProfileForm extends StatefulWidget {
  const PageProfileForm({Key? key}) : super(key: key);

  @override
  State<PageProfileForm> createState() => _PageProfileFormState();
}

class _PageProfileFormState extends State<PageProfileForm> {
  final ImagePicker _picker = ImagePicker();
  final TextEditingController maxWidthController = TextEditingController();
  final TextEditingController maxHeightController = TextEditingController();
  final TextEditingController qualityController = TextEditingController();

  XFile? _imageFile; // 갤러리에서 사진 선택한것 소스코드.. 일단 null로 있다가 선택된걸 집어넣는 변수.

  String _currentImageUrl = ''; // api에서 이미지 주소를 보내주면 정보를 담아둘 변수
  String _imageUploadTime = ''; // api에서 업로드 시간을 보내주면 정보를 담아둘 변수

  @override
  void initState() {
    super.initState();
    _getCurrentImage(); // 페이지 들어오자마자 현재 프로필 이미지 정보 가져옴.
  }

  // 현재 프로필 이미지 정보 가져오는 메서드
  Future<void> _getCurrentImage() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoProfile().getImage().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _currentImageUrl = res.data!.imageName;
        _imageUploadTime = res.data!.dateUpload;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarActions(
        title: '프로필 사진 관리',
      ),
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: Center(
        child: Container(
          margin: EdgeInsets.all(30.0),
          padding: EdgeInsets.all(30.0),
          child: Column(
            children: [
              if (_currentImageUrl == '')
                Container(
                  child: Text('프로필 사진이 없습니다.'),
                ),
              if (_currentImageUrl != '')
                Container(
                  child: Image.network(_currentImageUrl),
                ),
              if (_currentImageUrl != '')
                Container(
                  child: Text('업로드시간 : $_imageUploadTime'),
                ),
              SizedBox(height: 30,),
              OutlinedButton(
                  onPressed: () {
                    _callGallery(context: context);
                  },
                  child: const Text('이미지 선택') // 이미지 선택 버튼은 항상 보이게
              ),
            ],
          ),
        )
      )
    );
  }
  /*
   핸드폰 갤러리 띄워서 사용자에게 사진 선택을 요구하고 사용자(사람)가 사진을 선택하면
   api로 사진을 전송해서 이미지 업로드를 요구함.
   */
  Future<void> _callGallery({BuildContext? context}) async {
    try {
      final XFile? pickedFile = await _picker.pickImage(
        source: ImageSource.gallery,
        maxWidth: 2000, // 갤러리에서 선택할 사진의 최대 폭 (폭이나 넓이가 크다 = 사진용량이 크다 = 서버터진다)
        maxHeight: 2000, // 갤러리에서 선택할 사진의 최대 높이
        imageQuality: 100, // 사진퀄리티 (0~100, 높을수록 사진 용량 늘어남)
      );
      setState(() {
        // 사진선택이 완료되면 선택된 사진을 사진리스트 변수에 넣어둠 : 타입 XFile
        _imageFile = pickedFile;
      });

      File file = File(_imageFile!.path); // XFile(패키지에서 쓰는 자체 타입) 을 File로 변환함.
      _doUploadImage(file); // 사진 파일을 업로드 시킴
    } catch (e) {
      ComponentNotification(
        success: false,
        title: '갤러리 호출 취소',
        subTitle: '갤러리 호출을 취소하였습니다.',
      ).call();
    }
  }

  Future<void> _doUploadImage(File file) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoProfile().doUpload(file).then((res) {
      BotToast.closeAllLoading();

      _getCurrentImage(); // 사진 업로드에 성공하면 다시 한번 api로 현재 프로필 이미지 정보를 달라고 요청함.
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '이미지 업로드 실패',
        subTitle: '이미지 업로드에 실패하였습니다.',
      ).call();
    });
  }
}
