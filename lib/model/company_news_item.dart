class CompanyNewsItem {
  int id;
  String headline;
  String contents;

  CompanyNewsItem(this.id, this.headline, this.contents);

  factory CompanyNewsItem.fromJson(Map<String, dynamic> json) {
    return CompanyNewsItem(
        json['id'],
        json['headline'],
        json['contents']);
  }
}