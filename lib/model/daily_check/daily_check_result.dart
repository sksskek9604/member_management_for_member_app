import 'package:member_management_for_member_app/model/daily_check/daily_check_response.dart';

class DailyCheckResult {
  bool isSuccess;
  int code;
  String msg;
  DailyCheckResponse data;

  DailyCheckResult(this.isSuccess, this.code, this.msg, this.data);

  factory DailyCheckResult.fromJson(Map<String, dynamic> json) {
    return DailyCheckResult(
      json['isSuccess'],
      json['code'],
      json['msg'],
      DailyCheckResponse.fromJson(json['data']),
    );
  }
}