import 'package:member_management_for_member_app/model/daily_check/daily_check_list_item.dart';

class DailyCheckListResult {

  int totalItemCount;
  int totalPage;
  int currentPage;
  bool isSuccess;
  int code;
  String msg;
  List<DailyCheckListItem> list;

  DailyCheckListResult(
      this.totalItemCount,
      this.totalPage,
      this.currentPage,
      this.isSuccess,
      this.code,
      this.msg,
      this.list);

  factory DailyCheckListResult.fromJson(Map<String, dynamic> json) {
    return DailyCheckListResult(
      json['totalItemCount'] as int,
      json['totalPage'] as int,
      json['currentPage'] as int,
      json['isSuccess'] as bool,
      json['code'] as int,
      json['msg'],
      json['list'] == null ? [] : (json['list'] as List).map((e) => DailyCheckListItem.fromJson(e)).toList(),
    );
  }


}