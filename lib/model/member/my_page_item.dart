class MyPageItem {
  int id;
  bool isAdmin;
  String username;
  String password;
  String fullName;
  String gender;
  String memberPhone;
  String memberAddress;
  String dateJoin;

  MyPageItem(this.id, this.isAdmin, this.username, this.password, this.fullName, this.gender, this.memberPhone, this.memberAddress, this.dateJoin);

  factory MyPageItem.fromJson(Map<String, dynamic> json) {
    return MyPageItem(
      json['id'],
      json['isAdmin'],
      json['username'],
      json['password'],
      json['fullName'],
      json['gender'],
      json['memberPhone'],
      json['memberAddress'],
      json['dateJoin'],
    );
  }
} // Todo dart파일 이름 바꾸기