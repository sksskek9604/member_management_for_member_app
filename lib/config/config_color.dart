import 'package:flutter/material.dart';

const Color colorOnBoardingGradientStart = Color.fromARGB(255, 51, 189, 220);
const Color colorOnBoardingGradientEnd = Color.fromARGB(255, 38, 88, 218);

const Color colorPrimary = Color.fromARGB(255, 38, 104, 221);
const Color colorSecondary = Color.fromARGB(255, 251, 106, 28);
const Color colorRed = Color.fromARGB(255, 216, 34, 34);
const Color colorGray = Color.fromRGBO(0, 0, 0, 0.35);
const Color colorDarkGray = Color.fromRGBO(0, 0, 0, 0.65);
const Color colorLightGray = Color.fromRGBO(0, 0, 0, 0.1);
