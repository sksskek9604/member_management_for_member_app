import 'package:member_management_for_member_app/model/member/my_page_item.dart';

class MyPageResult {
  bool isSuccess;
  int code;
  String msg;
  MyPageItem data;

  MyPageResult(this.isSuccess, this.code, this.msg, this.data);

  factory MyPageResult.fromJson(Map<String, dynamic> json) {
    return MyPageResult(
      json['isSuccess'],
      json['code'],
      json['msg'],
      MyPageItem.fromJson(json['data']),
    );
  }
}