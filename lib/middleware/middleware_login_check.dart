import 'package:flutter/material.dart';
import 'package:member_management_for_member_app/functions/token_lib.dart';
import 'package:member_management_for_member_app/model/member/login_user_response.dart';
import 'package:member_management_for_member_app/pages/page_Index.dart';
import 'package:member_management_for_member_app/pages/page_login.dart';

class MiddleWareLoginCheck {
  void check(BuildContext context) async {
    LoginUserResponse? token = await TokenLib.getToken();

    /*
    최초에 앱을 실행하면 token은 null이고
    한번이라도 로그아웃을 했다면 빈문자열임. (로그아웃 시 setToken에서 ''로 삭제해버림)
    token이 없으면 비회원용 메인으로 강제이동
    token이 있으면 회원용 메인으로 강제이동
     */

    if (token == null || token == '') {
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageLogin()), (route) => false);
    } else { // 로그인 필요, 어플 초기화면
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageIndex()), (route) => false);
      // 로그인 다음 기본페이지
    }
  }
}