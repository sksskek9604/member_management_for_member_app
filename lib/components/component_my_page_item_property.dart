import 'package:flutter/material.dart';
import 'package:member_management_for_member_app/config/config_size.dart';

class ComponentMyPageItemProperty extends StatelessWidget {
  const ComponentMyPageItemProperty({super.key, required this.title, required this.contents});

  final String title;
  final String contents;


  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(5),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            decoration: BoxDecoration(
              border: Border.all(width: 2.0, color: Colors.teal),
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: Column(
              children: [
                const SizedBox(height: 10,),
                Text(title,
                  style: const TextStyle(
                    fontSize: fontSizeBig,
                    fontWeight: FontWeight.bold,
                  ),),
                Divider(),
                const SizedBox(height: 10,),
                Text(contents,
                  style: const TextStyle(
                    fontSize: fontSizeMid,
                    color: Colors.teal,
                  ),),
                const SizedBox(height: 10,)
              ],
            ),
          ),
        ],
      ),
    );
  }
}

