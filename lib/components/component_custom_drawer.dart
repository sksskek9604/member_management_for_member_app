import 'dart:io';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:member_management_for_member_app/components/component_custom_loading.dart';
import 'package:member_management_for_member_app/components/component_notification.dart';
import 'package:member_management_for_member_app/functions/token_lib.dart';
import 'package:member_management_for_member_app/pages/page_Index.dart';
import 'package:member_management_for_member_app/pages/page_dailycheck.dart';
import 'package:member_management_for_member_app/pages/page_dailycheck_my_list.dart';
import 'package:member_management_for_member_app/pages/page_my_page.dart';
import 'package:member_management_for_member_app/pages/page_profile_form.dart';
import 'package:member_management_for_member_app/pages/page_vacation_list.dart';
import 'package:member_management_for_member_app/pages/page_vacation_request.dart';
import 'package:member_management_for_member_app/repository/repo_profile.dart';
import 'package:slider_button/slider_button.dart';
import 'package:widget_circular_animator/widget_circular_animator.dart';

class ComponentCustomDrawer extends StatefulWidget {
  const ComponentCustomDrawer({Key? key}) : super(key: key);

  @override
  State<ComponentCustomDrawer> createState() => _ComponentCustomDrawerState();
}

class _ComponentCustomDrawerState extends State<ComponentCustomDrawer> {

  final ImagePicker _picker = ImagePicker();
  final TextEditingController maxWidthController = TextEditingController();
  final TextEditingController maxHeightController = TextEditingController();
  final TextEditingController qualityController = TextEditingController();

  XFile? _imageFile; // 갤러리에서 사진 선택한것 소스코드.. 일단 null로 있다가 선택된걸 집어넣는 변수.

  String _currentImageUrl = '';
  String _imageUploadTime = '';

  @override
  void initState() {
    super.initState();
    _getCurrentImage(); // 페이지 들어오자마자 현재 프로필 이미지 정보 가져옴.
  }

  // Todo 업로드 기능 수정 할 것
  // 현재 프로필 이미지 정보 가져오는 메서드
  Future<void> _getCurrentImage() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoProfile().getImage().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _currentImageUrl = res.data!.imageName;
        _imageUploadTime = res.data!.dateUpload;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();
    });
  }

  Future<void> _callGallery({BuildContext? context}) async {
    try {
      final XFile? pickedFile = await _picker.pickImage(
        source: ImageSource.gallery,
        maxWidth: 2000, // 갤러리에서 선택할 사진의 최대 폭 (폭이나 넓이가 크다 = 사진용량이 크다 = 서버터진다)
        maxHeight: 2000, // 갤러리에서 선택할 사진의 최대 높이
        imageQuality: 100, // 사진퀄리티 (0~100, 높을수록 사진 용량 늘어남)
      );
      setState(() {
        // 사진선택이 완료되면 선택된 사진을 사진리스트 변수에 넣어둠 : 타입 XFile
        _imageFile = pickedFile;
      });

      File file = File(_imageFile!.path); // XFile(패키지에서 쓰는 자체 타입) 을 File로 변환함.
      _doUploadImage(file); // 사진 파일을 업로드 시킴
    } catch (e) {
      ComponentNotification(
        success: false,
        title: '갤러리 호출 취소',
        subTitle: '갤러리 호출을 취소하였습니다.',
      ).call();
    }
  }

  Future<void> _doUploadImage(File file) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoProfile().doUpload(file).then((res) {
      BotToast.closeAllLoading();

      _getCurrentImage(); // 사진 업로드에 성공하면 다시 한번 api로 현재 프로필 이미지 정보를 달라고 요청함.
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '이미지 업로드 실패',
        subTitle: '이미지 업로드에 실패하였습니다.',
      ).call();
    });
  }

  Future<void> _logout(BuildContext context) async {
    TokenLib.logout(context);
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: Colors.teal.shade400,
      child: Container(
        child: ListTileTheme(
          tileColor: Colors.black12,
          textColor: Colors.white,
          iconColor: Colors.white,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: [
              SizedBox(height: 50,),
              Center(
                child: Column(
                  children: [
                    if (_currentImageUrl == '')
                      CircleAvatar(
                        radius: 100,
                        child: Icon(Icons.account_circle, size: 200,),
                      ),
                    if (_currentImageUrl != '')
                      CircleAvatar(
                        radius: 100,
                        backgroundImage: NetworkImage(_currentImageUrl),
                      ),
                  ],
                ),
              ),
              SizedBox(height: 30,),
              Column(
                children: [
                  SliderButton(
                    width: 260,
                    height: 60,
                    action: () {
                      _logout(context);
                    },
                    label: Text(
                      "Always be happy!",
                      style: TextStyle(
                          color: Color(0xff4a4a4a),
                          fontWeight: FontWeight.w500,
                          fontSize: 17),
                    ),
                    icon: Center(
                        child: Icon(
                          Icons.power_settings_new,
                          color: Colors.teal,
                          size: 30.0,
                          semanticLabel: 'drawer 로그아웃 슬라이드 버튼',
                        )),
                  ),
                ],
              ),
              SizedBox(height: 20,),
              ListTile(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => PageIndex())
                  );
                },
                leading: Icon(Icons.home),
                title: Text('홈'),
              ),
              ListTile(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => PageMyPage())
                  );
                },
                leading: Icon(Icons.account_circle_rounded),
                title: Text('마이 페이지'),
              ),
              ListTile(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => PageDailyCheck())
                  );
                },
                leading: Icon(Icons.add_location),
                title: Text('출/퇴근 등록'),
              ),
              ListTile(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => PageDailyCheckMyList())
                  );
                },
                leading: Icon(Icons.storage),
                title: Text('출/퇴근 기록 조회'),
              ),
              ListTile(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => PageVacationRequest())
                  );
                },
                leading: Icon(Icons.airplanemode_active),
                title: Text('휴가 신청'),
              ),
              ListTile(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => PageVacationList())
                  );
                },
                leading: Icon(Icons.airplanemode_active),
                title: Text('휴가 기록/잔여 조회'),
              ),
              Spacer(),
              DefaultTextStyle(
                style: TextStyle(
                  fontSize: 12,
                  color: Colors.white54,
                ),
                child: Container(
                  margin: const EdgeInsets.symmetric(
                    vertical: 16.0,
                  ),
                  child: Text('Terms of Service | Privacy Policy'),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
