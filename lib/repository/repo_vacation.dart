import 'package:dio/dio.dart';
import 'package:member_management_for_member_app/config/config_api.dart';
import 'package:member_management_for_member_app/functions/token_lib.dart';
import 'package:member_management_for_member_app/model/common_result.dart';
import 'package:member_management_for_member_app/model/member/login_user_response.dart';
import 'package:member_management_for_member_app/model/vacation_list_result.dart';
import 'package:member_management_for_member_app/model/vacation_request.dart';

class RepoVacation {

  Future<CommonResult> setData(VacationRequest request) async {
    const String baseUrl = '$apiUri/vacation/post-memberVacation/{memberId}';

    LoginUserResponse? token = (await TokenLib.getToken()) as LoginUserResponse?;

    Dio dio = Dio();

    final response = await dio.post(
        baseUrl.replaceAll('{memberId}', token!.memberId.toString()),
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }

  Future<VacationListResult> getVacationApprovalList({String approvalStatus = ''}) async {
    const String baseUrl = '$apiUri/vacation/approval-status/{memberId}';

    LoginUserResponse? token = (await TokenLib.getToken()) as LoginUserResponse?;

    Map<String, dynamic> params = {};
    if (approvalStatus != '') params['approvalStatus'] = approvalStatus;

    Dio dio = Dio();

    final response = await dio.get(
        baseUrl.replaceAll('{memberId}', token!.memberId.toString()),
        queryParameters: params,

        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return VacationListResult.fromJson(response.data);

  } //Todo 승인 타입 별 조회 리스트 가져오기


  Future<VacationListResult> getVacationMemberListAll() async {
    const String baseUrl = '$apiUri/vacation/all/{memberId}';

    Dio dio = Dio();

    LoginUserResponse? token = (await TokenLib.getToken());

    final response = await dio.get(
      baseUrl.replaceAll('{memberId}', token!.memberId.toString()),
      options: Options(
          followRedirects: false,
          validateStatus: (status) => status == 200),
    );
    return VacationListResult.fromJson(response.data);
  }


}