# 사원관리(직원용) Android APP

### * member-management를 API로 사용하는 APP입니다.
### * 사원용/ 관리자용 app에서 사원용 app의 디자인입니다.

---

## 초기 디자인

<figure class="main">
    <img src="./images/first_login.jpg", width="100", >
    <img src="./images/first_home.jpg", width="100">
    <img src="./images/first_mypage.jpg", width="100">

<ce class="main">
<img src="./images/first_daily_check_request_or_list.jpg", width="100">
<img src="./images/first_dailycheck_gps.jpg", width="100">
<img src="./images/first_daily_check_list.jpg", width="100">

figure>

## Login_ Page
> ##
> <center><img src="./images/login.jpg" width="450" height="900"></center>
> 

## Drawer
> #####
> 
> <center><img src="./images/drawer.jpg" width="500" height="900"></center>
>

## My_Page
> #####
>
> <center><img src="./images/my_page.jpg" width="500" height="900"></center>
>


## Home(Company_news) 
> #####
> 
> <center><img src="./images/company_news_home.jpg" width="500" height="900"></center>


## Daily_check
> #####
> 
> <center><img src="./images/daily_check.png" width="550" height="900"></center>

## Daily_check_list
> #####
> 
> > <center><img src="./images/daily_check_list.jpg" width="500" height="900"></center>

## Vacation_request
> #####
> 
> > <center><img src="./images/vacation_request.png" width="550" height="900"></center>

## Vacation_list
> #####
> 
> > <center><img src="./images/vacation_list.jpg" width="500" height="900"></center>


