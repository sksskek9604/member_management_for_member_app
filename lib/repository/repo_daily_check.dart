import 'package:dio/dio.dart';
import 'package:member_management_for_member_app/config/config_api.dart';
import 'package:member_management_for_member_app/functions/token_lib.dart';
import 'package:member_management_for_member_app/model/daily_check/daily_check_result.dart';
import 'package:member_management_for_member_app/model/daily_check/daily_chehck_list_result.dart';
import 'package:member_management_for_member_app/model/member/login_user_response.dart';


class RepoDailyCheck{
  Future<DailyCheckResult> getData() async {
    const String baseUrl = '$apiUri/daily-check/status/member-id/{memberId}';

    Dio dio = Dio();

    LoginUserResponse? token = (await TokenLib.getToken()) as LoginUserResponse?;

    final response = await dio.get(
        baseUrl.replaceAll('{memberId}', token!.memberId.toString()),
        options: Options(
            followRedirects: false
        )
    );
    return DailyCheckResult.fromJson(response.data);
  }

  Future<DailyCheckResult> putState(String dailyCheckState) async {
    const String baseUrl = '$apiUri/daily-check/status/{dailyCheckState}/member-id/{memberId}';

    Dio dio = Dio();

    LoginUserResponse? token = (await TokenLib.getToken()) as LoginUserResponse?;

    final response = await dio.put(
        baseUrl.replaceAll('{dailyCheckState}', dailyCheckState).replaceAll('{memberId}', token!.memberId.toString()), //.replaceAll 두가지 필요했던 것!!
        options: Options(
            followRedirects: false
        )
    );
    return DailyCheckResult.fromJson(response.data);
  }


  Future<DailyCheckListResult> getList() async {
    const String baseUrl = '$apiUri/daily-check/status/all/member-id/{memberId}';

    Dio dio = Dio();

    LoginUserResponse? token = (await TokenLib.getToken());

    final response = await dio.get(
        baseUrl.replaceAll('{memberId}', token!.memberId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return DailyCheckListResult.fromJson(response.data);
  }
}