import 'package:member_management_for_member_app/model/company_news_item.dart';

class CompanyNewsListResult {

  int totalItemCount;
  int totalPage;
  int currentPage;
  bool isSuccess;
  int code;
  String msg;
  List<CompanyNewsItem> list;

  CompanyNewsListResult(
      this.totalItemCount,
      this.totalPage,
      this.currentPage,
      this.isSuccess,
      this.code,
      this.msg,
      this.list
      );

  factory CompanyNewsListResult.fromJson(Map<String, dynamic> json) {
    return CompanyNewsListResult(
      json['totalItemCount'] as int,
      json['totalPage'] as int ,
      json['currentPage']as int ,
      json['isSuccess'] as bool ,
      json['code'] as int,
      json['msg'],
      json['list'] == null? [] : (json['list'] as List).map((e) => CompanyNewsItem.fromJson(e)).toList(),

    );
  }
}