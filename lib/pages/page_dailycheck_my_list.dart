import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_drawer/flutter_advanced_drawer.dart';
import 'package:member_management_for_member_app/components/component_count_title.dart';
import 'package:member_management_for_member_app/components/component_custom_drawer.dart';
import 'package:member_management_for_member_app/components/component_custom_loading.dart';
import 'package:member_management_for_member_app/components/component_daily_check_data_item.dart';
import 'package:member_management_for_member_app/components/component_notification.dart';
import 'package:member_management_for_member_app/model/daily_check/daily_check_list_item.dart';
import 'package:member_management_for_member_app/repository/repo_daily_check.dart';

class PageDailyCheckMyList extends StatefulWidget {
  const PageDailyCheckMyList({Key? key}) : super(key: key);

  @override
  State<PageDailyCheckMyList> createState() => _PageDailyCheckMyListState();
}

class _PageDailyCheckMyListState extends State<PageDailyCheckMyList> {

  final _advancedDrawerController = AdvancedDrawerController();
  final _scrollController = ScrollController();

  List<DailyCheckListItem> _list = [ ];
  int _totalItemCount = 0;

  @override
  void initState() {
    super.initState();
    _getDailyCheckList();
  }

  Future<void> _getDailyCheckList() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoDailyCheck().getList().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _list = res.list;
        _totalItemCount = res.totalItemCount;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '데이터 로딩에 실패하였습니다.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return AdvancedDrawer(
        backdropColor: Colors.teal,
        controller: _advancedDrawerController,
        animationCurve: Curves.easeInOut,
        animationDuration: const Duration(milliseconds: 300),
        animateChildDecoration: true,
        rtlOpening: false,
        disabledGestures: false,
        childDecoration: const BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(16)),
        ),
        drawer: ComponentCustomDrawer(),
        child: Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                  image: DecorationImage(
                      opacity: 1.0,
                      fit: BoxFit.fill,
                      image: AssetImage('assets/background.png'))),
              child: Scaffold(
                backgroundColor: Colors.transparent,
                appBar: AppBar(
                  title: const Text('오늘도 좋은 하루 되세요!'),
                  leading: IconButton(
                    onPressed: _handleMenuButtonPressed,
                    icon: ValueListenableBuilder<AdvancedDrawerValue>(
                      valueListenable: _advancedDrawerController,
                      builder: (_, value, __) {
                        return AnimatedSwitcher(
                          duration: Duration(milliseconds: 300),
                          child: Icon(
                            value.visible ? Icons.arrow_back_ios : Icons.menu,
                            key: ValueKey<bool>(value.visible),
                          ),
                        );
                      },
                    ),
                  ),
                  actions: [
                    IconButton(onPressed: () {
                      _getDailyCheckList();
                    }, icon: Icon(Icons.refresh))
                  ],
                ),
                body: ListView(
                  controller: _scrollController,
                  children: [
                    ComponentCountTitle(icon: Icons.wb_incandescent, count: _totalItemCount, unitName: '건', itemName: '출퇴근 기록',),
                    _buildBody(context),
                  ],
                ),
              ),
            )
          ],
        )
    );
  }

  Widget _buildBody(BuildContext context) {
    if (_totalItemCount > 0) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (_, index) => ComponentDailyCheckDataItem(
              dailyCheckListItem: _list[index],
            ),
          ),
        ],
      );
    } else {
      return SizedBox(
        height: MediaQuery
            .of(context)
            .size
            .height - 45,
        child: IconButton(
            onPressed: () {
              _getDailyCheckList();
            }, icon: Icon(Icons.refresh)),
      );
    }
  }
  void _handleMenuButtonPressed() {
    _advancedDrawerController.showDrawer();
  }
}
