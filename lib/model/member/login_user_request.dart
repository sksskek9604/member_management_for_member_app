class LoginUserRequest{

  String username;
  String password;

  LoginUserRequest(this.username, this.password);

  // fromJson은 json을 가져다가 이 객체로 변경한 것
  // request는 왜 객체 자체를 json으로 변환해야할까?
  // 스웨거에 있는 request = json이기 때문에, json으로 줘야함.
  // 객체는 메모리 상에서만 모양을 잡고 감, json은예쁘게 보여지는 것이기 때문
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    data['username'] = this.username;
    data['password'] = this.password;

    return data;
  }
}