class VacationItem {

  int? vacationTotalUsageId;

  String? vacationApplyRequestSimpleInfo;

  String? vacationApplyValueInfo;

  VacationItem({this.vacationTotalUsageId,this.vacationApplyRequestSimpleInfo, this.vacationApplyValueInfo});

  factory VacationItem.fromJson(Map<String, dynamic> json) {
    return VacationItem(
      vacationTotalUsageId: json['vacationTotalUsageId'] != null ? json['vacationTotalUsageId'] : '-',
      vacationApplyRequestSimpleInfo:  json['vacationApplyRequestSimpleInfo'] != null ? json['vacationApplyRequestSimpleInfo'] : '-',
      vacationApplyValueInfo: json['vacationApplyValueInfo'] != null ? json['vacationApplyValueInfo'] : '-'

    );
  }
}