import 'package:flutter/material.dart';
import 'package:member_management_for_member_app/model/vacation_item.dart';

class ComponentVacationItem extends StatelessWidget {
  const ComponentVacationItem({super.key, required this.vacationItem});

  final VacationItem vacationItem;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(width: 2.0, color: Colors.black12),
          borderRadius: BorderRadius.circular(20.0),
        ),
        padding: EdgeInsets.only(top: 10, left: 40, right: 40, bottom: 30),
        margin: EdgeInsets.all(30),
        child: Column(
          children: [
            SizedBox(height: 30,),
            Text('${vacationItem.vacationApplyRequestSimpleInfo}', style: TextStyle(fontSize:32, fontWeight: FontWeight.w500, color: Colors.teal.shade500),),
            SizedBox(height: 30,),
            Text('${vacationItem.vacationApplyValueInfo}', style: TextStyle(fontSize: 22)),
          ],
        ),
      ),
    );
  }
}
