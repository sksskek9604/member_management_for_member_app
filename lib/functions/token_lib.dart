import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:member_management_for_member_app/components/component_notification.dart';
import 'package:member_management_for_member_app/model/member/login_user_response.dart';
import 'package:member_management_for_member_app/pages/page_login.dart';
import 'package:shared_preferences/shared_preferences.dart';


/*
SharedPreferences 에 데이터가 저장되는 방식은 Map임을 반드시 숙지하고 갈 것.
Map이 무엇인지 알아야 함.
 */

class TokenLib {

  // token 세팅 (등록하면 끝이니까 void, 기다릴 필요도 없음, 주면 끝이다.)
  static void setToken(LoginUserResponse response) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('memberId', response.memberId.toString()); // 토큰 형태 변경, response에서 받아옴
  }
  // token 가져오기
  static Future<LoginUserResponse?> getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String? memberId = prefs.getString('memberId');

    if (memberId == null) {
      return null;
    } else {
      return LoginUserResponse(int.parse(memberId));
    }
  }

  // 로그아웃 만들 것 -> 비회원용 메인페이지로 가야함.
  // 메모리에 있는 토큰 값을 지우고, 강제로 비회원 메인페이지로 이동시킨다.
  static void logout(BuildContext context) async {
    // 기존에 닫혀있던 토스트팝업, 커스텀로딩 다 닫고
    BotToast.closeAllLoading();

    // 메모리에 접근해서 key 가 token인 값을 빈문자열('')로 바꿔버리고
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('token', '');

    // 토스트메시지 띄우고
    ComponentNotification(
      success: false,
      title: '로그아웃',
      subTitle: '로그아웃되어 로그인 페이지로 이동합니다.', // 해킹때문에 요즘 추세는 뭉뜽그림
    ).call();

    // 지금까지 했던 페이지들 히스토리 싹 다 지우고, 새거 하나만 나와 라는 뜻 = 유일한 히스토리 1개가 됨.
    // page_index(비회원용 메인)으로 강제로 이동시킨다.
    Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageLogin()), (route) => false);
  }
}