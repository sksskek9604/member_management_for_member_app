class DailyCheckListItem {
  String? dailyCheckState;

  String? dateBase;

  String? dateWorkStart;

  String? dateShortOuting;

  String? dateWorkComeBack;

  String? dateEarlyLeave;

  String? dateWorkEnd;

  String? dateUpdate;

  DailyCheckListItem(
      {this.dailyCheckState,
      this.dateBase,
      this.dateWorkStart,
      this.dateShortOuting,
      this.dateWorkComeBack,
      this.dateEarlyLeave,
      this.dateWorkEnd,
      this.dateUpdate});

  factory DailyCheckListItem.fromJson(Map<String, dynamic> json) {
    return DailyCheckListItem(
      dailyCheckState:
          json['dailyCheckState'] != null ? json['dailyCheckState'] : '-',
      dateBase: json['dateBase'] != null ? json['dateBase'] : '-',
      dateWorkStart:
          json['dateWorkStart'] != null ? json['dateWorkStart'] : '-',
      dateShortOuting:
          json['dateShortOuting'] != null ? json['dateShortOuting'] : '-',
      dateWorkComeBack:
          json['dateWorkComeBack'] != null ? json['dateWorkComeBack'] : '-',
      dateEarlyLeave:
          json['dateEarlyLeave'] != null ? json['dateEarlyLeave'] : '-',
      dateWorkEnd: json['dateWorkEnd'] != null ? json['dateWorkEnd'] : '-',
      dateUpdate: json['dateUpdate'] != null ? json['dateUpdate'] : '-',
    );
  }
}
