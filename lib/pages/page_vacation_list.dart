import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_advanced_drawer/flutter_advanced_drawer.dart';
import 'package:member_management_for_member_app/components/component_count_title.dart';
import 'package:member_management_for_member_app/components/component_custom_drawer.dart';
import 'package:member_management_for_member_app/components/component_custom_loading.dart';
import 'package:member_management_for_member_app/components/component_no_contents.dart';
import 'package:member_management_for_member_app/components/component_notification.dart';
import 'package:member_management_for_member_app/components/component_vacation_item.dart';
import 'package:member_management_for_member_app/model/vacation_item.dart';
import 'package:member_management_for_member_app/repository/repo_vacation.dart';

class PageVacationList extends StatefulWidget {
  const PageVacationList({Key? key}) : super(key: key);

  @override
  State<PageVacationList> createState() => _PageVacationListState();
}

class _PageVacationListState extends State<PageVacationList> {
  final _scrollController = ScrollController();
  final _advancedDrawerController = AdvancedDrawerController();

  List<VacationItem> _list = [];
  int _totalItemCount = 0;

  @override
  void initState() {
    super.initState();
    _getListAll();
  }

  // Future<void> _getList() async {
  //   BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
  //     return ComponentCustomLoading(cancelFunc: cancelFunc);
  //   });
  //
  //   await RepoVacation().getVacationList().then((res) {
  //     BotToast.closeAllLoading();
  //
  //     setState(() {
  //       _list = res.list;
  //       _totalItemCount = res.totalItemCount;
  //     });
  //   }).catchError((err) {
  //     BotToast.closeAllLoading();
  //
  //     // 이번에 추가된 알림창 컴포넌트. component_notification 참고
  //     ComponentNotification(
  //       success: false,
  //       title: '데이터 로딩 실패',
  //       subTitle: '데이터 로딩에 실패하였습니다.',
  //     ).call();
  //   });
  // }

  Future<void> _getListAll() async {
    BotToast.showCustomLoading(
        toastBuilder: (cancelFunc)
        => ComponentCustomLoading(cancelFunc: cancelFunc)
    );

    await RepoVacation().getVacationMemberListAll().then((res) {
      BotToast.closeAllLoading();

      setState(() {
        _list = res.list;
        _totalItemCount = res.totalItemCount;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '불러오기 실패',
        subTitle: '데이터 불러오기에 실패했습니다.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return AdvancedDrawer(
        backdropColor: Colors.teal,
        controller: _advancedDrawerController,
        animationCurve: Curves.easeInOut,
        animationDuration: const Duration(milliseconds: 300),
        animateChildDecoration: true,
        rtlOpening: false,
        disabledGestures: false,
        childDecoration: const BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(16)),
        ),
        drawer: ComponentCustomDrawer(),
        child: Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                  image: DecorationImage(
                      opacity: 1.0,
                      fit: BoxFit.fill,
                      image: AssetImage('assets/background.png'))),
             child: Scaffold(
               backgroundColor: Colors.transparent,
               appBar: AppBar(
                 title: const Text('오늘도 행복한 하루 보내세요!'),
                 leading: IconButton(
                   onPressed: _handleMenuButtonPressed,
                   icon: ValueListenableBuilder<AdvancedDrawerValue>(
                     valueListenable: _advancedDrawerController,
                     builder: (_, value, __) {
                       return AnimatedSwitcher(
                         duration: Duration(milliseconds: 250),
                         child: Icon(
                           value.visible ? Icons.arrow_back_ios : Icons.menu,
                           key: ValueKey<bool>(value.visible),
                         ),
                       );
                     },
                   ),
                 ),
                 actions: [
                   IconButton(onPressed: () {
                     _getListAll();
                   }, icon: Icon(Icons.refresh))
                 ],
               ),
               body: ListView(
                 controller: _scrollController,
                 children: [
                   ComponentCountTitle(icon: Icons.wb_incandescent, count: _totalItemCount, unitName: '건', itemName: '휴가 신청 내역'),
                   _buildBody(),
                 ],
               ),
             ),
            )
          ],
        )
    );
  }

  Widget _buildBody() {
    if (_totalItemCount > 0){
      return Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (_, index) => ComponentVacationItem(
              vacationItem: _list[index],
            ),
          ),
        ],
      );
    } else {
      return SizedBox(
        height: MediaQuery.of(context).size.height - 45,
        child: const ComponentNoContents(icon: Icons.not_interested, msg: '데이터가 없습니다.'),
      );
    }
  }
  void _handleMenuButtonPressed() {
    _advancedDrawerController.showDrawer();
  }
  //Todo count 해올 것, 승인 필터 만들어주기
}
