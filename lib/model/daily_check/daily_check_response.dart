class DailyCheckResponse {
  String dailyCheckStateName;
  String dailyCheckState;

  DailyCheckResponse(this.dailyCheckStateName, this.dailyCheckState);


  factory DailyCheckResponse.fromJson(Map<String,dynamic> json) {
    return DailyCheckResponse(
      json['dailyCheckStateName'],
      json['dailyCheckState']
    );
  }
}