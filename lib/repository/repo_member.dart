import 'package:member_management_for_member_app/config/config_api.dart';
import 'package:member_management_for_member_app/functions/token_lib.dart';
import 'package:dio/dio.dart';
import 'package:member_management_for_member_app/model/member/login_result.dart';
import 'package:member_management_for_member_app/model/member/login_user_request.dart';
import 'package:member_management_for_member_app/model/member/login_user_response.dart';
import 'package:member_management_for_member_app/model/member/my_page_result.dart';

class RepoMember {
  Future<LoginResult> doLogin(LoginUserRequest request) async {
    const String baseUrl = '$apiUri/login-member/login';

    Dio dio = Dio();

    final response = await dio.post(
        baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            })
    );
    return LoginResult.fromJson(response.data);
  }

  Future<MyPageResult> getMemberItem({int? memberId}) async {
    const String baseUrl = '$apiUri/login-member/my-page/{memberId}';

    Dio dio = Dio();

    LoginUserResponse? token = (await TokenLib.getToken()) as LoginUserResponse?;

    final response = await dio.get(
        baseUrl.replaceAll('{memberId}', token!.memberId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) => status == 200),
            );
    return MyPageResult.fromJson(response.data);
  }


}